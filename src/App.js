import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import AuthPageComponent from "./components/AuthPageComponent/AuthPageComponent";
import RatingPageComponent from "./components/RatingPageComponent/RatingPageComponent";
import GamePageComponent from "./components/GamePageComponent/GamePageComponent";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path={"/"}
          element={<AuthPageComponent></AuthPageComponent>}
        ></Route>
        <Route
          path={"/rating"}
          element={<RatingPageComponent></RatingPageComponent>}
        ></Route>
        <Route
          path={"/game"}
          element={<GamePageComponent></GamePageComponent>}
        ></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
