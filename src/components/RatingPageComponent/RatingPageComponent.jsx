import HeaderComponent from "../common/HeaderComponent/HeaderComponent";
import "./RatingPageComponent.scss";

export default function RatingPageComponent() {
  return (
    <>
      <HeaderComponent currentActiveTabNum={1}></HeaderComponent>
      <section className={"rating-section"}>
        <h1 className={"rating-section__headline"}>Рейтинг игроков</h1>
        <table className={"rating-section__table"}>
          <thead>
            <tr>
              <th className={"table__table-head-cell"}>ФИО</th>
              <th className={"table__table-head-cell"}>Всего игр</th>
              <th className={"table__table-head-cell"}>Победы</th>
              <th className={"table__table-head-cell"}>Проигрыши</th>
              <th className={"table__table-head-cell"}>Процент побед</th>
            </tr>
          </thead>
          <tbody>
            {ratingData.map((record) => {
              return (
                <tr key={record.id} className={"table__table-row"}>
                  <td className={"table__table-data-cell"}>{record.name}</td>
                  <td className={"table__table-data-cell"}>
                    {record.totalAmountOfGames}
                  </td>
                  <td className={"table__table-data-cell table__games-won"}>
                    {record.gamesWonAmount}
                  </td>
                  <td className={"table__table-data-cell table__games-lost"}>
                    {record.gamesLostAmount}
                  </td>
                  <td className={"table__table-data-cell"}>
                    {record.winsPercentage}%
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </section>
    </>
  );
}

const ratingData = [
  {
    name: "Александров Игнат Анатолиевич",
    totalAmountOfGames: 24534,
    gamesWonAmount: 9824,
    gamesLostAmount: 1222,
    winsPercentage: 87,
    id: 0,
  },
  {
    name: "Шевченко Рафаил Михайлович",
    totalAmountOfGames: 24534,
    gamesWonAmount: 9824,
    gamesLostAmount: 1222,
    winsPercentage: 87,
    id: 1,
  },
  {
    name: "Мазайло Трофим Артёмович",
    totalAmountOfGames: 24534,
    gamesWonAmount: 9824,
    gamesLostAmount: 1222,
    winsPercentage: 87,
    id: 2,
  },
  {
    name: "Логинов Остин Данилович",
    totalAmountOfGames: 24534,
    gamesWonAmount: 9824,
    gamesLostAmount: 1222,
    winsPercentage: 87,
    id: 3,
  },
  {
    name: "Борисов Йошка Васильевич",
    totalAmountOfGames: 24534,
    gamesWonAmount: 9824,
    gamesLostAmount: 1222,
    winsPercentage: 87,
    id: 4,
  },
  {
    name: "Соловьёв Ждан Михайлович",
    totalAmountOfGames: 24534,
    gamesWonAmount: 9824,
    gamesLostAmount: 1222,
    winsPercentage: 87,
    id: 5,
  },
  {
    name: "Негода Михаил Эдуардович",
    totalAmountOfGames: 24534,
    gamesWonAmount: 9824,
    gamesLostAmount: 1222,
    winsPercentage: 87,
    id: 6,
  },
  {
    name: "Гордеев Шамиль Леонидович",
    totalAmountOfGames: 24534,
    gamesWonAmount: 9824,
    gamesLostAmount: 1222,
    winsPercentage: 87,
    id: 7,
  },
  {
    name: "Многогрешный Павел Виталиевич",
    totalAmountOfGames: 24534,
    gamesWonAmount: 9824,
    gamesLostAmount: 1222,
    winsPercentage: 87,
    id: 8,
  },
  {
    name: "Александров Игнат Анатолиевич",
    totalAmountOfGames: 24534,
    gamesWonAmount: 9824,
    gamesLostAmount: 1222,
    winsPercentage: 87,
    id: 9,
  },
  {
    name: "Волков Эрик Алексеевич",
    totalAmountOfGames: 24534,
    gamesWonAmount: 9824,
    gamesLostAmount: 1222,
    winsPercentage: 87,
    id: 10,
  },
  {
    name: "Кузьмин Ростислав Васильевич",
    totalAmountOfGames: 24534,
    gamesWonAmount: 9824,
    gamesLostAmount: 1222,
    winsPercentage: 87,
    id: 11,
  },
  {
    name: "Стрелков Филипп Борисович",
    totalAmountOfGames: 24534,
    gamesWonAmount: 9824,
    gamesLostAmount: 1222,
    winsPercentage: 87,
    id: 12,
  },
  {
    name: "Галкин Феликс Платонович",
    totalAmountOfGames: 24534,
    gamesWonAmount: 9824,
    gamesLostAmount: 1222,
    winsPercentage: 87,
    id: 13,
  },
];
