import signOutIcon from "./signout-icon.svg";
import logo from "./s-logo.svg";
import "./HeaderComponent.scss";
export default function HeaderComponent({ currentActiveTabNum }) {
  return (
    <header className={"header"}>
      <div>
        <img src={logo} alt={'App logo. Consists of "xoxo" string'} />
      </div>
      <div className={"header__nav-panel"}>
        <div
          className={`header__nav-panel__menu-elem ${
            currentActiveTabNum === 0 && "header__nav-panel__menu-elem--active"
          }`}
        >
          Игровое поле
        </div>
        {/*  TODO: Вопрос. Как здесь лучше сделать активацию меню по пропсу?*/}
        <div
          className={`header__nav-panel__menu-elem ${
            currentActiveTabNum === 1 && "header__nav-panel__menu-elem--active"
          }`}
        >
          Рейтинг
        </div>
        <div
          className={`header__nav-panel__menu-elem ${
            currentActiveTabNum === 2 && "header__nav-panel__menu-elem--active"
          }`}
        >
          Активные игроки
        </div>
        <div
          className={`header__nav-panel__menu-elem ${
            currentActiveTabNum === 3 && "header__nav-panel__menu-elem--active"
          }`}
        >
          История игр
        </div>
        <div
          className={`header__nav-panel__menu-elem ${
            currentActiveTabNum === 4 && "header__nav-panel__menu-elem--active"
          }`}
        >
          Список игроков
        </div>
      </div>
      <button className={"header__sign-out-button"}>
        <img src={signOutIcon} alt={"Sign-out icon"} />
      </button>
    </header>
  );
}
