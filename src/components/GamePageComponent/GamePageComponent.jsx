import "./GamePageComponent.scss";
import zero from "./zero.svg";
import x from "./x.svg";
import zero_xxl from "./xxl-zero.svg";
import x_xxl from "./xxl-x.svg";
import sendButton from "./send-btn.svg";
import HeaderComponent from "../common/HeaderComponent/HeaderComponent";
import { useState } from "react";

export default function GamePageComponent() {
  const [currentPlayer, setCurrentPlayer] = useState("x");
  const [gameField, setGameField] = useState(gameFieldInitial);

  function handleCellClick(index) {
    const rowNumber = Math.floor(index / 3);
    const columnNumber = index % 3;

    if (gameField[rowNumber][columnNumber] === null) {
      // Обновляем gameField state не изменяя массив, а создавая новый
      // TODO: Вопрос. Не слишком ли сложный алогритм? Может стоило сделать deep copy, а потом её
      // подставлять в setGame field? Можете подсказать?

      // setGameField(
      //   gameField.map((gameFieldRow, rowIndex) => {
      //     if (rowNumber === rowIndex) {
      //       return gameFieldRow.map((cell, cellIndex) =>
      //         cellIndex === columnNumber ? currentPlayer : cell
      //       );
      //     } else return gameFieldRow;
      //   })
      // );

      const nextGameField = structuredClone(gameField);
      nextGameField[rowNumber][columnNumber] = currentPlayer;
      setGameField(nextGameField);

      setCurrentPlayer(currentPlayer === "x" ? "o" : "x");
    }
  }

  return (
    <>
      <HeaderComponent currentActiveTabNum={0}></HeaderComponent>
      <div className="main-container">
        <div className="subject-list">
          <h1 className="subject-list__headline">Игроки</h1>
          <div className="subject-list__container">
            <div className="subject-list__subject-container">
              <div>
                <img
                  className="subject-list__subject-icon"
                  alt={"Zero icon"}
                  src={zero}
                />
              </div>
              <div className="subject-list__subject-info">
                <div className="subject-container__subject-name">
                  Пупкин Владелен Игоревич
                </div>
                <div className="subject-container__subject-percent">
                  63% побед
                </div>
              </div>
            </div>
            <div className="subject-list__subject-container">
              <div>
                <img
                  className="subject-list__subject-icon"
                  alt={"X icon"}
                  src={x}
                />
              </div>
              <div className="subject-list__subject-info">
                <div className="subject-container__subject-name">
                  Плюшкина Екатерина Викторовна
                </div>
                <div className="subject-container__subject-percent">
                  23% побед
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="game-container">
          <div className="game-container__game-time">05:12</div>
          <div className="game-container__game-board">
            {gameField.flat().map((cell, index) => {
              return (
                <div
                  className={"game-board__cell"}
                  key={index}
                  onClick={() => handleCellClick(index)}
                >
                  {cell === "x" && (
                    <img
                      className={"game-board__img"}
                      alt={"Big x icon"}
                      src={x_xxl}
                    />
                  )}
                  {cell === "o" && (
                    <img
                      className={"game-board__img"}
                      alt={"Big o icon"}
                      src={zero_xxl}
                    />
                  )}
                </div>
              );
            })}
          </div>
          <div className="game-container__game-step">
            Ходит&nbsp;
            {currentPlayer === "x" ? (
              <>
                <img alt={"X icon"} src={x} />
                &nbsp;Плюшкина Екатерина
              </>
            ) : (
              <>
                <img alt={"Zero icon"} src={zero} />
                &nbsp;Владелен Пупкин
              </>
            )}
          </div>
        </div>
        <div className="chat-container">
          <div className="chat-container__msgs-container">
            {messages.map((message) => {
              return (
                <div
                  key={message.id}
                  className={`msgs-container__msg-container ${message.type}`}
                >
                  <div className="msg-container__msg-header">
                    <div
                      className={`msg-header__subject-name ${
                        message.type === "other" ? "x" : "zero"
                      }`}
                    >
                      {message.from}
                    </div>
                    <div className="msg-header__time">{message.time}</div>
                  </div>
                  <div className="msg-header__msg-body">{message.text}</div>
                </div>
              );
            })}
          </div>
          <div className="chat-container__msg-interactive-elements">
            <textarea
              className={"msg-interactive-elements__textarea"}
              placeholder="Сообщение..."
            ></textarea>
            <button className={"msg-interactive-elements__button"}>
              <img alt={"Send button icon"} src={sendButton} />
            </button>
          </div>
        </div>
      </div>
    </>
  );
}

const gameFieldInitial = [
  [null, null, "x"],
  ["o", "o", null],
  [null, "x", null],
];

const messages = [
  {
    type: "other",
    from: "Плюшкина Екатерина",
    time: "13:40",
    text: "Ну что, готовься к поражению!!1",
    id: 0,
  },
  {
    type: "me",
    from: "Пупкин Владлен",
    time: "13:41",
    text: "Надо было играть за крестики. Розовый — мой не самый счастливый цвет",
    id: 1,
  },
  {
    type: "me",
    from: "Пупкин Владлен",
    time: "13:45",
    text: "Я туплю...",
    id: 2,
  },
  {
    type: "other",
    from: "Плюшкина Екатерина",
    time: "13:47",
    text: "Отойду пока кофе попить, напиши в тг как сходишь",
    id: 3,
  },
];
